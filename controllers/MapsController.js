var debug = require('debug')('wallmart:controller');
function MapsController(MapsModel) {
	this.model = MapsModel;
}

//Controller para pegar todos os mapas carregados
MapsController.prototype.allMaps = function(req, res, next) {
	this.model.allMaps(function(err, data) {
		if(err) {
			console.log(err);
			return next(err);
		}
		res.json(data);
	});
};

//Controller para pegar um mapa especifico
MapsController.prototype.mapByName = function(req, res, next) {
	this.model.mapByName(req.params.nome, function(err, data) {
		if(err) {
			console.log(err);
			return next(err);
		}
		res.json(data);
	});
};

//Controller para inserir um mapa novo
MapsController.prototype.newMap = function(req, res, next) {	
	var query = {nome: req.body.nome, rotas: req.body.rotas, rotaDijkstra: formatDijkstra(req.body.rotas) }
	this.model.newMap(query, function(err, data) {
		if(err) {
			console.log(err);
			return next(err);
		}
		res.json(data);
	});
};

//Controller para calculo de rota
MapsController.prototype.route = function(req, res, next) {
	var query = {
		nome: req.body.mapa, 
		origem: req.body.origem, 
		destino: req.body.destino, 
		autonomia: req.body.autonomia,
		valor: req.body.valor 
	}

	this.model.route(query, function(err, data) {
		if(err) {
			console.log(err);
			return next(err);
		}
		res.json(data);
	});
};

//Controller para alterar um mapa
MapsController.prototype.updateMap = function(req, res, next) {
	var query = {nome: req.body.nome, rotas: req.body.rotas, rotaDijkstra: formatDijkstra(req.body.rotas) }

	this.model.updateMap(query, function(err, data) {
		if(err) {
			console.log(err);
			return next(err);
		}
		res.json(data);
	});
};

//Controller para excluir um mapa
MapsController.prototype.deleteMap = function(req, res, next) {
	this.model.deleteMap(req.params.nome, function(err, data) {
		if(err) {
			console.log(err);
			return next(err);
		}
		res.json(data);
	});
};

function formatDijkstra (rota) {
	//formating request for Dijkstra's algorithm.
	var route = [];
	var obj = {};
	for (i=0; i < rota.length; i++ ) {
		route.push(rota[i].split(" "));
		obj[route[i][0]] = {};
	}

	for (var origin in obj) {	
		for (y=0; y < route.length; y++) {
			if (origin == route[y][0]) {
				obj[origin][route[y][1]] = route[y][2];
			}
		}
	}

	return obj;
	//END - formating request for Dijkstra's algorithm.
}

module.exports = function(MapsModel) {
	return new MapsController(MapsModel);
};