var express		= require('express');
	router 		= express.Router();

var mongoose 		= require('../db/mongoose');
var MapsModel 		= require('../models/MapsModel')(mongoose);
var MapsController 	= require('../controllers/MapsController')(MapsModel);

//rota para pegar todos os mapas carregados *devolve o grafo tbm
router.get('/', MapsController.allMaps.bind(MapsController));

//rota para pegar um mapa especifico *não devolve o grafo
router.get('/:nome', MapsController.mapByName.bind(MapsController));

//rota para inserir um mapa novo
router.post('/new', MapsController.newMap.bind(MapsController));

//rota para calculo de menor caminho
router.post('/route', MapsController.route.bind(MapsController));

//rota para alterar um mapa
router.put('/update', MapsController.updateMap.bind(MapsController));

//rota para excluir um mapa
router.delete('/delete/:nome', MapsController.deleteMap.bind(MapsController));

module.exports = router;