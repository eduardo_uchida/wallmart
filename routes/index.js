'use strict';

var express = require('express'),
    router  = express.Router();

router.get('/', function(req, res, next) {
  res.render('index');
});

router.use('/maps', require('./maps'));

module.exports = router;