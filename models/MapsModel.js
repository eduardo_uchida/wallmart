'use strict'

function MapsDAO(model) {
	this.model = model;
}

//Model para pegar todos os mapas carregados
MapsDAO.prototype.allMaps = function(callback) {
	this.model.find(function(err, result) {
		// console.log(result);
		if(err) {
			callback(err, null);
		} else {
			var response = {
				success: true,
        data: result
			}
			callback(null, response);
		}
	});
};

//Model para pegar um mapa especifico
MapsDAO.prototype.mapByName = function(name, callback) {
	this.model.findOne( { "nome" : name }, function(err, result) {
		// console.log(result)
		if(err) {
			callback(err, null);
		} else if (!result) {
			var response = {
				success: false,
                message: "Não encontramos nenhum mapa com o nome informado."
			}
			callback(null, response);
		} else {
			var response = {
				success: true,
        data: {
        	"nome": result.nome,
        	"rotas": result.rotas
        }
			}
			callback(null, response);
		}
	});
};

//Model para inserir um mapa novo
MapsDAO.prototype.newMap = function(data, callback) {
	this.model.create( data, function(err, result) {
		// console.log(result);
		if (err) {
			callback(err, null);
		} else {
			var response = {
				success: true,
       	data: "O mapa foi inserido no sistema com sucesso."
			}
			callback(null, response);		
		}
	});
};

//Model para calculo de rota
MapsDAO.prototype.route = function(data, callback) {
	this.model.findOne({"nome" : data.nome}, function(err, result) {
		if (err) {
			callback(err, null);
		} else {
			const Graph = require('node-dijkstra')
			const route = new Graph(result.rotaDijkstra);
			var p = route.path(data.origem, data.destino, { cost: true })
			var custo = (p.cost / data.autonomia * data.valor);
			
			var response = {
				success: true,
        		data: {"rota": p.path , "custo": custo}
			}
			callback(null, response);		
		}
	});
};

//Model para alterar um mapa
MapsDAO.prototype.updateMap = function(data, callback) {
	this.model.update(
		{"nome" : data.nome},
		{"$set" : {"nome": data.nome, "rotas": data.rotas, "rotaDijkstra": data.rotaDijkstra}},
		{"new": true}, function(err, result) {
		if (err) {
			callback(err, null);
		} else if (!data) {
			var retorno = {
        success: false,
        message: "Não existe nenhum mapa com o nome informado."
      }
      callback(null, retorno);
		} else {
			var retorno = {
        success: true,
        message: "Mapa alterado com sucesso."
      }
      callback(null, retorno);
		}
	});
};

//Model para excluir um mapa
MapsDAO.prototype.deleteMap = function(name, callback) {
	this.model.findOneAndRemove({ "nome" : name } , function(err, data) {
    if (err) {
      callback(err, null);
    } else if (!data) {
      var retorno = {
        success: false,
        message: "Não existe nenhum mapa com o nome informado."
      }
      callback(null, retorno);
    } else {
      var retorno = {
        success: true,
        message: "Mapa excluído com sucesso."
      }
      callback(null, retorno);
    }
  });
};

module.exports = function(mongoose) {
	var Map = mongoose.model('Map', {
		nome: {type: String, unique : true, dropDups: true, required: true},
  	rotas: {type: Array, required: false},
  	rotaDijkstra: {type: Object, required: false}
	});
	return new MapsDAO(Map);
}