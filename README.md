# README #
* Esse projeto foi desenvolvido por Eduardo Massami Uchida, como um teste para a vaga número 52744 presente na APINFO.
* Verão 0.0.1
* A tecnologia utilizada foi o NODEJS com conexão com o MongoDB (mLab), decidiu-se por tal pela forma como o NodeJS e o MongoDB trabalham bem entre si (JSON e BSON) e a facilidade em se criar um servidor com o NodeJS.


### Subindo o Projeto ###
**Configuração**

* Para subir o projeto será necessário instalar o NPM e o NODE, segue link com instruções para tal [http://nodebr.com/instalando-node-js-atraves-do-gerenciador-de-pacotes/](Link URL).
Com o NODE e o NPM instalados digite em um terminal **npm install -g nodemon**, clone o projeto, entre na pasta **cd wallmart** e digite o seguinte comando **npm install**.

Ao final da instalação digite **nodemon** no seu terminal e a aplicação estará pronta para uso.

**Dependências**

* As depencias do projeto estão listadas no package.json

**Configuração do Banco**

* A base está no mLab, caso deseje testar com outra base, edite os arquivo presentes na pasta config (default.json e test.json).

**Testes**

* Para rodar testes, vá para o terminal, acesse o diretório do projeto e digite **npm test**.

### Funcionamento ###
O servidor tem 6 rotas criadas:

**http://localhost:3000/maps/**

Função: Rota para devolver todos os mapas carregados no sistema.

Verbo HTTP: GET

Parâmetro: Nenhum


**http://localhost:3000/maps/:nome**

Função: Rota para devolver um mapa específico.

Verbo HTTP: GET

Parâmetro: o nome do mapa (:nome)


**http://localhost:3000/maps/new**

Função: Rota para inserir um mapa.

Verbo HTTP: POST

Parâmetro: JSON como o exemplo abaixo

{
  "nome":"SP",
  "rotas":[
    "A B 10",
    "B D 15",
    "A C 20",
    "C D 30",
    "B E 50",
    "D E 30"
  ]
}


**http://localhost:3000/maps/route**

Função: Rota para informar qual o caminho de menor custo.

Verbo HTTP: POST

Parâmetro: JSON como o exemplo abaixo

{
  "mapa": "SP", 
  "origem": "A", 
  "destino": "D", 
  "autonomia": 10,
  "valor": 2.50 
}


**http://localhost:3000/maps/update**

Função: Rota para alterar um mapa.

Verbo HTTP: PUT

Parâmetro: JSON como o exemplo abaixo

{
  "nome":"SP",
  "rotas":[
    "A B 10",
    "B D 15",
    "A C 20",
    "C D 30",
    "B E 50",
    "D E 30"
  ]
}


**http://localhost:3000/maps/delete/:nome**

Função: Rota para excluir um mapa.

Verbo HTTP: DELETE

Parâmetro: o nome do mapa (:nome)


### Contato ###
* Eduardo Massami Uchida

email: massami.uchida@gmail.com

tel: (11) 98423-3057