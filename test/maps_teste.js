var request = require('supertest');
var assert = require('assert');
var config = require('config');
var db = require('../db/mongoose.js');

var app = require('../app');

function insert(callback) {
  var mapa = {
    "nome":"MG",
    "rotaDijkstra": {
        "D": {
            "E": "30"
        },
        "C": {
            "D": "30"
        },
        "B": {
            "E": "50",
            "D": "15"
        },
        "A": {
            "C": "20",
            "B": "10"
        }
    },
    "rotas":[
      "A B 10",
      "B D 15",
      "A C 20",
      "C D 30",
      "B E 50",
      "D E 30"
    ]
  };

  db.connection.collections['maps'].insert(mapa, callback);
}


describe('Maps Routes Tests', function () {
  var nome;
  var id;

  // limpando banco de teste
  beforeEach(function (done) {
    
    db.connection.collections['maps'].remove({}, function(err, data) {
      if (err) {
        console.log("Erro ao conectar no banco de teste!");
        done();
      }
      else {
        insert(function(err, data) {
          nome = data.ops[0].nome;
          id = data.ops[0]._id;
          done();
        });
      }
    });
    
  });

  //Teste de rota para criação de novo mapa.
  it('POST /maps/new', function(done) {
    var mapa = {
      "nome":"SP",
      "rotas":[
        "A B 10",
        "B D 15",
        "A C 20",
        "C D 30",
        "B E 50",
        "D E 30"
      ]
    }

    request(app)
      .post('/maps/new')
      .send(mapa)
      .end(function(err, response) {

        var body = response.body;

        assert.equal(200, response.status);
        assert.equal(true, body.success);
        assert.equal('O mapa foi inserido no sistema com sucesso.', body.data);

        done();
      });
  });

  it('GET /maps/:nome', function(done){
    request(app)
      .get('/maps/' + nome)
      .end(function(err, response) {
        var body = response.body;

        assert.equal(200, response.status);
        assert.equal(true, body.success);
        assert.deepEqual(body.data, { 
                                 "nome": "MG",
                                  "rotas":[
                                    "A B 10",
                                    "B D 15",
                                    "A C 20",
                                    "C D 30",
                                    "B E 50",
                                    "D E 30"
                                  ] 
                                });
        done();
      })
  });

  //teste para devolver todos os mapas
  it('GET /maps', function(done){
    request(app)
      .get('/maps')
      .end(function(err, response) {
        var body = response.body;

        assert.equal(200, response.status);
        assert.equal(true, body.success);
        assert.deepEqual(body.data[0], { 
                                  "_id" : String(id),
                                 "nome": "MG",
                                 "rotaDijkstra": {
                                      "D": {
                                          "E": "30"
                                      },
                                      "C": {
                                          "D": "30"
                                      },
                                      "B": {
                                          "E": "50",
                                          "D": "15"
                                      },
                                      "A": {
                                          "C": "20",
                                          "B": "10"
                                      }
                                  },
                                  "rotas":[
                                    "A B 10",
                                    "B D 15",
                                    "A C 20",
                                    "C D 30",
                                    "B E 50",
                                    "D E 30"
                                  ] 
                                });
        done();

    })
  });

  it('PUT /update', function(done){
    var mapa = {
      "nome": nome,
      "rotas":[
        "A B 14",
        "B D 15",
        "A C 20",
        "C D 30",
        "B E 50",
        "D E 30"
      ]
    }

    request(app)
      .put('/maps/update')
      .send(mapa)
      .end(function(err, response) {
        var body = response.body;
        var expected = { success: true, message: 'Mapa alterado com sucesso.' };

        assert.deepEqual(body, expected);

        done();
      })
  });

  it('DELETE /delete/:nome', function(done) {
    request(app)
      .delete('/maps/delete/' + nome)
      .end(function(err,response) {
        var body = response.body;
        var expected = {"message": "Mapa excluído com sucesso.", "success": true};

        assert.deepEqual(body, expected);

        done();
      });
  });

  it('GET /maps/:nome not found', function(done){
    request(app)
    .get('/maps/111111111111111111111111')
    .end(function(err, response) {
      var body = response.body;
      var expected = {"success":false,"message":"Não encontramos nenhum mapa com o nome informado."};
      assert.deepEqual(body, expected);
      done();
    });
  });

});